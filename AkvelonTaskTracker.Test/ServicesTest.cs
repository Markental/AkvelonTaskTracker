using AkvelonTaskTracker.Core.Services;
using System;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using AkvelonTaskTracker.Core.ContextEntities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System.Linq;

namespace AkvelonTaskTracker.Test
{
    public class ServicesTest
    {
        private readonly IProjectInfoService _projectInfoService;
        private readonly ApplicationDbContext _context;
        public ServicesTest() 
        {
            var services = new ServiceCollection();

            services
                .AddTransient<IProjectInfoService, ProjectInfoService>()
                .AddDbContext<ApplicationDbContext>(options => 
                {
                    options.UseInMemoryDatabase("TestDb");
                });

            var serviceProvider = services
                .BuildServiceProvider();
            _context = serviceProvider.GetService<ApplicationDbContext>();
            _projectInfoService = serviceProvider.GetService<IProjectInfoService>();
            Seed().GetAwaiter().GetResult();
        }
        
        public async Task Seed() 
        {
            var hasStatuses = _context.TaskStatuses.Any();
            if (!hasStatuses)
            {
                _context.TaskStatuses.Add(new TaskInfoStatus
                {
                    Name = "To Do",
                    Flag = Core.Abstractions.Enums.TaskStatusEnum.ToDo,
                });
                _context.TaskStatuses.Add(new TaskInfoStatus
                {
                    Name = "In Progress",
                    Flag = Core.Abstractions.Enums.TaskStatusEnum.InProgress,
                });
                _context.TaskStatuses.Add(new TaskInfoStatus
                {
                    Name = "Done",
                    Flag = Core.Abstractions.Enums.TaskStatusEnum.Done,
                });
            }

            hasStatuses = _context.ProjectStatuses.Any();
            if (!hasStatuses)
            {
                _context.ProjectStatuses.Add(new ProjectInfoStatus
                {
                    Name = "Not Started",
                    Flag = Core.Abstractions.Enums.ProjectStatusEnum.NotStarted,
                });
                _context.ProjectStatuses.Add(new ProjectInfoStatus
                {
                    Name = "Active",
                    Flag = Core.Abstractions.Enums.ProjectStatusEnum.Active,
                });
                _context.ProjectStatuses.Add(new ProjectInfoStatus
                {
                    Name = "Completed",
                    Flag = Core.Abstractions.Enums.ProjectStatusEnum.Completed,
                });
            }

            await _context.SaveChangesAsync();
        }

        [Fact]
        public async Task ProjectInfoService_Delete_EmptyId_False()
        {
            // Arrange
            var project = new ProjectInfo
            {
                Id = Guid.Empty,
                Name = "",
                Priority = 1
            };

            // Act
            var result = await _projectInfoService.Delete(project.Id);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task ProjectInfoService_GetProject_NotEmpty() 
        {
            var projectStatuses = await _projectInfoService.GetProjectStatuses();

            Assert.NotEmpty(projectStatuses);
        }
    }
}
