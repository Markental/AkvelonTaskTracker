# Akvelon Task Tracker
Test task for .Net internship in Akvelon
#### Published on Azure
- [Web Api]
- [Angular Client Application]

## Tecnologies used:
- ASP .Net Core 5
- Database - MS SQL
- Swagger
- [Angular] for client app

## Setup
[Angular] application ([Node.js npm] needed)
1. Clone repository
2. Open terminal in project folder
3. Enter command in terminal:
    ```
    npm install
    ```
4. After all packages are installed, write:
    ```
    ng serve
    ```
5. Go to given link (http://localhost:4200/ by default)

#### Contact me
- [LinkedIn]
- [Telegram]
- E-mail: [karataevolzhas@gmail.com]


[Node.js npm]: <https://nodejs.org/en/>
[Telegram]: <https://t.me/Markental>
[LinkedIn]: <https://www.linkedin.com/in/olzhas-karatayev/>
[Angular]: <https://angular.io/>
[karataevolzhas@gmail.com]: <mailto:karataevolzhas@gmail.com>
[Web Api]: <https://akvelontasktracker.azurewebsites.net/swagger/index.html>
[Angular Client Application]: <https://akvelontasktrackerclient.azurewebsites.net/>
