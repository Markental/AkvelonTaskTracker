﻿using AkvelonTaskTracker.Core.ContextEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkvelonTaskTracker.Core.Services
{
    public class ProjectInfoService : IProjectInfoService
    {
        private readonly ApplicationDbContext _context;

        public ProjectInfoService(ApplicationDbContext context) 
        {
            _context = context;
        }

        public async Task<ProjectInfo> GetOneById(Guid id)
        {
            var project = await _context.Projects
                .Include(p => p.ProjectStatus)
                .Include(p => p.Tasks)
                .ThenInclude(x => x.TaskStatus)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id && x.IsDeleted == false);
            
            foreach (var item in project.Tasks.ToList())
            {
                if (item.IsDeleted)
                {
                    project.Tasks.Remove(item);
                }
            }

            return project;
        }

        /// <summary>
        /// Filter by various parameters
        /// </summary>
        /// <param name="name">Filter by name</param>
        /// <param name="statusId">Filter by status</param>
        /// <param name="dateFrom">Filter from date</param>
        /// <param name="dateTo">Filter to date</param>
        /// <param name="pageNumber">Number of a page for pagination</param>
        /// <param name="pageSize">Number of elements in a page for pagination</param>
        /// <returns>Filtered collection and number of total items in it</returns>
        public async Task<(IEnumerable<ProjectInfo> Collection, int TotalCount)> GetFilteredList(
            string name,
            Guid? statusId, 
            DateTime? dateFrom,
            DateTime? dateTo, 
            int pageNumber, 
            int pageSize)
        {
            IQueryable<ProjectInfo> request = _context.Projects.Where(x => x.IsDeleted==false);

            if (!string.IsNullOrWhiteSpace(name)) 
            {
                string NameLower = name.ToLower();
                request = request.Where(x => x.Name.ToLower().Contains(NameLower));
            }

            if (statusId != null) 
            {
                request = request
                    .Where(x => x.ProjectStatusId == statusId);
            }

            if (dateFrom != null)
            {
                request = request
                    .Where(x => x.StartDate >= dateFrom);
            }

            if (dateTo != null)
            {
                request = request
                    .Where(x => x.StartDate <= dateTo);
            }

            var totalCount = request.Count();

            // Skip and top -> pagination
            if (!(pageNumber == -1))
            {
                request = request.Skip(pageNumber * pageSize);
            }
            if (!(pageSize == -1))
            {
                request = request.Take(pageSize);
            }

            var result = await request
                .Include(x => x.ProjectStatus)
                .OrderByDescending(x => x.Priority)
                .AsNoTracking()
                .ToListAsync();

            return (result, totalCount);
        }

        public async Task<Guid> Create(string name, int priority)
        {
            var status = await _context.ProjectStatuses
                .FirstOrDefaultAsync(x => x.Flag == Abstractions.Enums.ProjectStatusEnum.NotStarted);

            var project = new ProjectInfo
            {
                Name = name,
                Priority = priority,
                ProjectStatusId = status.Id
            };

            _context.Projects.Add(project);

            await _context.SaveChangesAsync();

            return project.Id;
        }

        public async Task<bool> Edit(Guid id, string name, int? priority)
        {
            var project = await _context.Projects
                .FirstOrDefaultAsync(x => x.Id == id);

            bool isChanged = false;

            if (project == null) 
            {
                return isChanged;
            }

            if (!string.IsNullOrWhiteSpace(name)) 
            {
                project.Name = name;
                isChanged = true;
            }

            if (priority != null)
            {
                project.Priority = priority.Value;
                isChanged = true;
            }

            await _context.SaveChangesAsync();

            return isChanged;
        }

        public async Task<bool> Delete(Guid Id) 
        {
            bool success = false;

            var project = await _context.Projects
                .Include(x => x.Tasks)
                .FirstOrDefaultAsync(x => x.Id == Id);

            if (project != null) 
            {
                if (project.IsDeleted)
                    return success;

                project.IsDeleted = true;
                foreach (var task in project.Tasks) 
                {
                    task.IsDeleted = true;
                }
                await _context.SaveChangesAsync();
                success = true;
            }

            return success;
        }

        public async Task<bool> MarkAsComplete(Guid Id) 
        {
            bool success = false;

            var project = await _context.Projects
                .Include(x => x.Tasks)
                .FirstOrDefaultAsync(x => x.Id == Id);

            if (project != null)
            {
                var statusComplete = await _context.ProjectStatuses
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Flag == Abstractions.Enums.ProjectStatusEnum.Completed);
                var statusDone = await _context.TaskStatuses
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Flag == Abstractions.Enums.TaskStatusEnum.Done);

                // Complete project
                project.ProjectStatusId = statusComplete.Id;

                // Complete tasks
                foreach (var task in project.Tasks)
                {
                    task.TaskStatusId = statusDone.Id;
                }
                await _context.SaveChangesAsync();
                success = true;
            }
            return success;
        }

        public async Task<IEnumerable<ProjectInfoStatus>> GetProjectStatuses() 
        {
            var statuses = await _context.ProjectStatuses
                .AsNoTracking()
                .ToListAsync();

            return statuses;
        }

    }

}
