﻿using AkvelonTaskTracker.Core.ContextEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkvelonTaskTracker.Core.Services
{
    public interface IProjectInfoService
    {
        Task<ProjectInfo> GetOneById(Guid id);
        Task<(IEnumerable<ProjectInfo> Collection, int TotalCount)> GetFilteredList(string name, Guid? statusId, DateTime? dateFrom, DateTime? dateTo, int pageNumber, int pageSize);
        Task<Guid> Create(string name, int priority);
        Task<bool> Edit(Guid id, string name, int? priority);
        Task<IEnumerable<ProjectInfoStatus>> GetProjectStatuses();
        Task<bool> Delete(Guid Id);
        Task<bool> MarkAsComplete(Guid Id);
    }
}
