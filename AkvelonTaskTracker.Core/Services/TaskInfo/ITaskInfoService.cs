﻿using AkvelonTaskTracker.Core.ContextEntities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AkvelonTaskTracker.Core.Services
{
    public interface ITaskInfoService
    {
        Task<Guid> Create(Guid projectId, string Name, string Description, int Priority);
        Task<bool> Delete(Guid id);
        Task<bool> Edit(Guid id, string name, string description, int? priority);
        Task<TaskInfo> GetOneById(Guid Id);
        Task<IEnumerable<TaskInfo>> GetTasksByProjectId(Guid Id);
        Task<IEnumerable<TaskInfoStatus>> GetTaskStatuses();
        Task<bool> MarkAsDone(Guid id);
        Task<bool> MarkAsInProgress(Guid id);
    }
}
