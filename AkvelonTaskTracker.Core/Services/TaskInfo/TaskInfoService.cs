﻿using AkvelonTaskTracker.Core.ContextEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkvelonTaskTracker.Core.Services
{
    public class TaskInfoService : ITaskInfoService
    {
        private readonly ApplicationDbContext _context;

        public TaskInfoService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<TaskInfo> GetOneById(Guid Id)
        {
            var task = await _context.Tasks
                .Include(x => x.TaskStatus)
                .Include(x => x.Project)
                .ThenInclude(x => x.ProjectStatus)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == Id && x.IsDeleted == false);

            return task;
        }

        public async Task<IEnumerable<TaskInfo>> GetTasksByProjectId(Guid Id)
        {
            var tasks = await _context.Tasks
                .Where(x => x.ProjectId == Id && x.IsDeleted == false)
                .Include(x => x.TaskStatus)
                .AsNoTracking()
                .OrderByDescending(x => x.Priority)
                .ToListAsync();

            return tasks;
        }

        public async Task<Guid> Create(Guid projectId, string name, string description, int priority)
        {
            var project = await _context.Projects
                .Include(x => x.Tasks)
                .FirstOrDefaultAsync(x => x.Id == projectId);

            if (project == null)
            {
                return Guid.Empty;
            }

            var statusToDo = await _context.TaskStatuses
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Flag == Abstractions.Enums.TaskStatusEnum.ToDo);

            var task = new TaskInfo
            {
                Name = name,
                Description = description,
                Priority = priority,

                TaskStatusId = statusToDo.Id,
                ProjectId = projectId
            };

            _context.Tasks.Add(task);

            await _context.SaveChangesAsync();

            return task.Id;
        }

        public async Task<bool> Edit(Guid id, string name, string description, int? priority) 
        {
            var task = await _context.Tasks
                .FirstOrDefaultAsync(x => x.Id == id);

            bool isChanged = false;

            if (task == null)
            {
                return isChanged;
            }

            if (!string.IsNullOrWhiteSpace(name))
            {
                task.Name = name;
                isChanged = true;
            }

            if (!string.IsNullOrWhiteSpace(description))
            {
                task.Description = description;
                isChanged = true;
            }

            if (priority != null)
            {
                task.Priority = priority.Value;
                isChanged = true;
            }

            await _context.SaveChangesAsync();

            return isChanged;
        }

        public async Task<bool> Delete(Guid id) 
        {
            bool success = false;

            var task = await _context.Tasks
                .FirstOrDefaultAsync(x => x.Id == id);

            if (task!=null)
            {
                if (task.IsDeleted)
                    return success;

                task.IsDeleted = true;
                await _context.SaveChangesAsync();
                success = true;
            }

            return success;
        }

        public async Task<bool> MarkAsInProgress(Guid id) 
        {
            bool success = false;

            var task = await _context.Tasks
                .Include(x => x.Project)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (task != null)
            {
                var statusInProgress = await _context.TaskStatuses
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Flag == Abstractions.Enums.TaskStatusEnum.InProgress);

                task.TaskStatusId = statusInProgress.Id;

                // set project status Active and assign StartDate
                if (task.Project.StartDate == null)
                {
                    var statusActive = await _context.ProjectStatuses
                        .AsNoTracking()
                        .FirstOrDefaultAsync(x => x.Flag == Abstractions.Enums.ProjectStatusEnum.Active);

                    task.Project.ProjectStatusId = statusActive.Id;
                    task.Project.StartDate = DateTime.Now;
                }

                await _context.SaveChangesAsync();

                success = true;
            }

            return success;
        }

        public async Task<bool> MarkAsDone(Guid id)
        {
            bool success = false;

            var task = await _context.Tasks
                .Include(x => x.Project)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (task != null)
            {
                var statusDone = await _context.TaskStatuses
                    .AsNoTracking()
                    .FirstOrDefaultAsync(x => x.Flag == Abstractions.Enums.TaskStatusEnum.Done);

                task.TaskStatusId = statusDone.Id;
                await _context.SaveChangesAsync();
                success = true;
            }

            return success;
        }

        public async Task<IEnumerable<TaskInfoStatus>> GetTaskStatuses()
        {
            var statuses = await _context.TaskStatuses
                .AsNoTracking()
                .ToListAsync();

            return statuses;
        }
    }
}
