﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AkvelonTaskTracker.Core.Models
{
    public class TaskVM
    {
        [Required]
        public Guid ProjectId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public int Priority { get; set; }
    }
}
