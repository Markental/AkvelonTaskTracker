﻿using System.ComponentModel.DataAnnotations;

namespace AkvelonTaskTracker.Core.Models
{
    public class ProjectVM
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public int Priority { get; set; }
    }
}
