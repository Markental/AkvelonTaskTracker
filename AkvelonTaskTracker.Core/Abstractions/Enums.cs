﻿namespace AkvelonTaskTracker.Core.Abstractions
{
    public class Enums
    {
        public enum ProjectStatusEnum 
        {
            /// <summary>
            /// Work on this project is not started yet
            /// </summary>
            NotStarted = 1,

            /// <summary>
            /// Work on this project is in progress
            /// </summary>
            Active = 2,

            /// <summary>
            /// Project is ready
            /// </summary>
            Completed = 3
        }

        public enum TaskStatusEnum
        {
            /// <summary>
            /// Task is defined
            /// </summary>
            ToDo = 1,

            /// <summary>
            /// Task is being carried out
            /// </summary>
            InProgress = 2,

            /// <summary>
            /// Task is done
            /// </summary>
            Done = 3
        }
    }
}
