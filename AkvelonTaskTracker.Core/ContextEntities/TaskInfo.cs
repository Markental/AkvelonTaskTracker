﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AkvelonTaskTracker.Core.ContextEntities
{
    [Table("Tasks")]
    public class TaskInfo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [MaxLength(200), Required]
        public string Name { get; set; }

        public string Description { get; set; }

        [ForeignKey("TaskStatus")]
        public Guid TaskStatusId { get; set; }
        public TaskInfoStatus TaskStatus { get; set; }

        [ForeignKey("Project")]
        public Guid ProjectId { get; set; }
        
        public virtual ProjectInfo Project {get;set;}

        public int Priority { get; set; }

        // Soft Delete Flag
        public bool IsDeleted { get; set; } = false;
    }
}
