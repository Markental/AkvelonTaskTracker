﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using static AkvelonTaskTracker.Core.Abstractions.Enums;

namespace AkvelonTaskTracker.Core.ContextEntities
{
    [Table("TaskStatuses")]
    public class TaskInfoStatus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [MaxLength(200), Required]
        public string Name { get; set; }

        /// <summary>
        /// Filtration Flag
        /// </summary>
        public TaskStatusEnum Flag { get; set; }

        public virtual IEnumerable<TaskInfo> Tasks { get; set; }
    }
}
