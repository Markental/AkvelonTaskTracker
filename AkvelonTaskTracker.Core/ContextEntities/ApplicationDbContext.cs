﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkvelonTaskTracker.Core.ContextEntities
{

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options)
        : base(options)
        {
        }

        protected ApplicationDbContext()
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<Project>()
            //    .HasMany(p => p.Tasks)
            //    .WithOne(t => t.Project);

            //modelBuilder.Entity<ProjectStatus>()
            //    .HasMany(s => s.Projects)
            //    .WithOne(p => p.ProjectStatus);

            //modelBuilder.Entity<TaskStatus>()
            //    .HasMany(s => s.Tasks)
            //    .WithOne(t => t.TaskStatus);

        }

        #region DbSets

        public DbSet<ProjectInfo> Projects { get; set; }

        public DbSet<TaskInfo> Tasks { get; set; }

        public DbSet<ProjectInfoStatus> ProjectStatuses { get; set; }

        public DbSet<TaskInfoStatus> TaskStatuses { get; set; }

        #endregion
    }
}
