﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AkvelonTaskTracker.Core.ContextEntities
{
    [Table("Projects")]
    public class ProjectInfo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [MaxLength(200), Required]
        public string Name { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? CompletionDate { get; set; }

        [ForeignKey("ProjectStatus")]
        public Guid ProjectStatusId { get; set; }
        public ProjectInfoStatus ProjectStatus { get; set; }

        /// <summary>
        /// List of Tasks
        /// </summary>
        public virtual IList<TaskInfo> Tasks { get; set; }

        public int Priority { get; set; }

        // Soft Delete Flag
        public bool IsDeleted { get; set; } = false;
    }
}
