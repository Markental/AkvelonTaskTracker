import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../services/http.service';
import { ProjectInfo } from '../models/ProjectInfo';

@Component({
  selector: 'app-task-create-dialog',
  templateUrl: './task-create-dialog.component.html',
  styleUrls: ['./task-create-dialog.component.css']
})
export class TaskCreateDialogComponent implements OnInit {

  taskForm!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<TaskCreateDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private fb: FormBuilder,
    private httpService: HttpService) {
      this.taskForm = this.fb.group({
        'Name': [""],
        'Description': [""],
        'Priority': [""]
      })
     }

  ngOnInit(): void {
  }

  cancel(): void {
    this.dialogRef.close();
  }

  create(): void {
    let Name = this.taskForm.get("Name")?.value;
    let Description = this.taskForm.get("Description")?.value;
    let Priority = this.taskForm.get("Priority")?.value;
    let data = new FormData();
    data.append("ProjectId", this.data.selectedProject.Id);
    data.append("Name", Name);
    data.append("Description", Description);
    data.append("Priority", Priority);
    this.httpService.createTask(data).subscribe(res => {
      this.httpService.getTask(res.Id).subscribe(res => {
        this.dialogRef.close(res);
      })
    });
  }
}
export interface DialogData {
  selectedProject: ProjectInfo;
}
