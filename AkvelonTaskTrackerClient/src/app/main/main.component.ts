import { Component, OnInit } from '@angular/core';
import { ProjectInfo } from '../models/ProjectInfo';
import { TaskInfo } from '../models/TaskInfo';
import { HttpService } from '../services/http.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ProjectCreateDialogComponent } from '../project-create-dialog/project-create-dialog.component';
import { TaskCreateDialogComponent } from '../task-create-dialog/task-create-dialog.component';
import { ProjectEditDialogComponent } from '../project-edit-dialog/project-edit-dialog.component';
import { DatePipe } from '@angular/common';
import { TaskEditDialogComponent } from '../task-edit-dialog/task-edit-dialog.component';
import { ProjectInfoStatus } from '../models/ProjectInfoStatus';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  projects: Array<ProjectInfo> = [];
  tasks: Array<TaskInfo> = [];
  projectStatuses: Array<ProjectInfoStatus> = []
  selectedProject!: ProjectInfo;
  selectedTask!: TaskInfo;
  statusNotStarted!: ProjectInfoStatus;
  statusActive!: ProjectInfoStatus;
  statusCompleted!: ProjectInfoStatus;
  constructor(private httpService: HttpService, public dialog: MatDialog) { }

  getAllProjects() {
    this.httpService.getProjects().subscribe(res => {
      this.projects = res;
    })
  }

  getNotStartedProjects() {
    this.httpService.getProjectsByStatusId(this.statusNotStarted.Id).subscribe(res => {
      this.projects = res;
    })
  }

  getActiveProjects() {
    this.httpService.getProjectsByStatusId(this.statusActive.Id).subscribe(res => {
      this.projects = res;
    })
  }

  getCompletedProjects() {
    this.httpService.getProjectsByStatusId(this.statusCompleted.Id).subscribe(res => {
      this.projects = res;
    })
  }

  ngOnInit(): void {
    this.getAllProjects()
    this.httpService.getProjectStatuses().subscribe(res => {
      this.projectStatuses = res;
      this.projectStatuses.forEach(element => {
        if (element.Name == "Not Started"){
          this.statusNotStarted = element;
        } 
        else if(element.Name == "Active"){
          this.statusActive = element;
        }
        else if(element.Name == "Completed"){
          this.statusCompleted = element;
        }
      });
    })
  }

  selectProject(Id: string) {
    this.projects.forEach(element => {
      if(element.Id == Id && this.selectedProject!=element) {
        this.selectedProject = element;
        console.log("selected project");
        console.log(this.selectedProject);
        this.selectedTask = undefined as any; // ???
        this.httpService.getTasksByProjectId(this.selectedProject.Id).subscribe(res => {
          this.tasks = res;
        })

      }
    });
  }

  selectTask(Id: string) {
    this.tasks.forEach(element => {
      if(element.Id == Id && this.selectedTask!=element) {
        this.selectedTask = element;
        console.log("selected task");
        console.log(this.selectedTask);
      }
    });
  }

  deleteProject() {
    this.httpService.deleteProject(this.selectedProject.Id).subscribe(res =>{
      this.projects.find((el, i) => {
        if(el.Id == this.selectedProject.Id){
          this.projects.splice(i, 1);

          this.selectedProject = null as any;
          this.selectedTask = null as any;

          this.projects.sort((a, b) => b.Priority - a.Priority);
          return true;
        }
        return false;
      })
    });
  }

  deleteTask() {
    this.httpService.deleteTask(this.selectedTask.Id).subscribe(res => {
      this.tasks.find((el, i) => {
        if(el.Id ==this.selectedTask.Id){
          this.tasks.splice(i, 1);

          this.selectedTask = null as any;

          this.tasks.sort((a, b) => b.Priority - a.Priority);
          return true;
        }
        return false;
      });
    });
  }

  projectComplete() {
    this.httpService.completeProject(this.selectedProject.Id).subscribe(res => {
      this.httpService.getProject(this.selectedProject.Id).subscribe(res => {
        this.selectedProject = res;
        this.tasks = this.selectedProject.Tasks;
        this.projects.find((el, i) => {
          if(el.Id == this.selectedProject.Id){
            this.projects.splice(i, 1, this.selectedProject);

            this.projects.sort((a, b) => b.Priority - a.Priority);
            return true;
          }
          return false;
        });
        this.selectedTask = null as any;
      });
    });
  }

  taskStartProgress() {
    this.httpService.taskInProgress(this.selectedTask.Id).subscribe(res => {
      this.httpService.getTask(this.selectedTask.Id).subscribe(res => {
        this.selectedTask = res;
        this.tasks.find((el, i) => {
          if(el.Id == this.selectedTask.Id){
            this.tasks.splice(i, 1, this.selectedTask);
            this.tasks.sort((a, b) => b.Priority - a.Priority);
            return true;
          }
          return false;
        });
      });
    });

    this.httpService.getProject(this.selectedProject.Id).subscribe(res => {
      this.selectedProject = res;
      this.projects.find((el, i) => {
        if(el.Id == this.selectedProject.Id){
          this.projects.splice(i, 1, this.selectedProject);

          this.projects.sort((a, b) => b.Priority - a.Priority);
          return true;
        }
        return false;
      });
    });
  }

  taskDone() {
    this.httpService.taskDone(this.selectedTask.Id).subscribe(res => {
      this.httpService.getTask(this.selectedTask.Id).subscribe(res => {
        this.selectedTask = res;
        this.tasks.find((el, i) => {
          if(el.Id == this.selectedTask.Id){
            this.tasks.splice(i, 1, this.selectedTask);
  
            this.tasks.sort((a, b) => b.Priority - a.Priority);
            return true;
          }
          return false;
        });
      });
    });
  }

  openCreateProjectDialog() {
    const dialogRef = this.dialog.open(ProjectCreateDialogComponent, {
      width: '800px',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(res => {
      this.projects.push(res);
      this.projects.sort((a, b) => b.Priority - a.Priority);
    });
  }
  openEditProjectDialog() {
    const dialogRef = this.dialog.open(ProjectEditDialogComponent, {
      width: '800px',
      data: {selectedProject: this.selectedProject},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(res => {
      this.selectedProject.Name = res.Name;
      this.selectedProject.Priority = res.Priority;
      
      this.projects.find((el, i) => {
        if(el.Id == this.selectedProject.Id){
          this.projects.splice(i, 1, this.selectedProject);

          this.projects.sort((a, b) => b.Priority - a.Priority);
          return true;
        }
        return false;
      });
      
    });
  }
   
  openCreateTaskDialog() {
    const dialogRef = this.dialog.open(TaskCreateDialogComponent, {
      width: '800px',
      data: {selectedProject: this.selectedProject},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(res => {
      this.tasks.push(res);
      this.tasks.sort((a, b) => b.Priority - a.Priority);
    });
  }
  openEditTaskDialog() {
    const dialogRef = this.dialog.open(TaskEditDialogComponent, {
      width: '800px',
      data: {selectedTask: this.selectedTask},
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(res => {
      this.selectedTask.Name = res.Name;
      this.selectedTask.Description = res.Description;
      this.selectedTask.Priority = res.Priority;
      
      this.tasks.find((el, i) => {
        if(el.Id == this.selectedTask.Id){
          this.tasks.splice(i, 1, this.selectedTask);

          this.tasks.sort((a, b) => b.Priority - a.Priority);
          return true;
        }
        return false;
      });
      
    });

  }

  formatedDate(createdOn: Date){
    let date = new DatePipe('en').transform(createdOn, 'yyyy/MM/dd')?.toString();
    return date;
  }
  formatedDateTime(createdOn: Date){
    let date = new DatePipe('en').transform(createdOn, 'yyyy/MM/dd HH:mm:ss')?.toString();
    return date;
  }
 
}
