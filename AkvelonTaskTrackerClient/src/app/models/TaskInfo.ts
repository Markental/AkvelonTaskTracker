import { ProjectInfo } from "./ProjectInfo";
import { TaskInfoStatus } from "./TaskInfoStatus";

export class TaskInfo {
    Id!: string;
    Name!: string;
    Description!: string;
    TaskStatusId!: string;
    TaskStatus!: TaskInfoStatus;
    ProjectId!: string;
    Project!: ProjectInfo;
    Priority!: number;
    IsDeleted!: boolean;
}