export interface TaskInfoStatus {
    Id: string,
    Name: string,
    Flag: number
}