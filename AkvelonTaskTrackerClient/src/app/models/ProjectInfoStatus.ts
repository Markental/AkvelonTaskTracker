export interface ProjectInfoStatus {
    Id: string,
    Name: string,
    Flag: number
}