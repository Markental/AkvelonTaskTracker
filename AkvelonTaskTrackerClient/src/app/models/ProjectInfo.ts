import { ProjectInfoStatus } from "./ProjectInfoStatus";
import { TaskInfo } from "./TaskInfo";

export interface ProjectInfo {
    Id: string,
    Name: string,
    StartDate: Date,
    CompletionDate: Date,
    ProjectStatusId: String,
    ProjectStatus: ProjectInfoStatus,
    Tasks: Array<TaskInfo>,
    Priority: number,
    IsDeleted: boolean
}