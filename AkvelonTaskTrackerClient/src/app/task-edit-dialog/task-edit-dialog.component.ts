import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../services/http.service';
import { TaskInfo } from '../models/TaskInfo';

@Component({
  selector: 'app-task-edit-dialog',
  templateUrl: './task-edit-dialog.component.html',
  styleUrls: ['./task-edit-dialog.component.css']
})
export class TaskEditDialogComponent implements OnInit {

  taskForm!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<TaskEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private fb: FormBuilder,
    private httpService: HttpService) {
      this.taskForm = this.fb.group({
        'Name': [this.data.selectedTask.Name],
        'Description': [this.data.selectedTask.Description],
        'Priority': [this.data.selectedTask.Priority]
      })
     }

  ngOnInit(): void {
  }

  cancel(): void {
    this.dialogRef.close();
  }

  edit(): void {
    let Name = this.taskForm.get("Name")?.value;
    let Description = this.taskForm.get("Description")?.value;
    let Priority = this.taskForm.get("Priority")?.value;

    this.httpService.editTask(this.data.selectedTask.Id, Name, Description, Priority).subscribe(res => {
      this.dialogRef.close({Name, Description, Priority});
    });
  }

}
export interface DialogData {
  selectedTask: TaskInfo;
}