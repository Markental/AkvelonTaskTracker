import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ProjectInfo } from '../models/ProjectInfo';
import { TaskInfo } from '../models/TaskInfo';
import { ProjectInfoStatus } from '../models/ProjectInfoStatus';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private apiPath = environment.apiUrl
  private projectsPath = environment.apiUrl + "ProjectInfo/"
  private tasksPath = environment.apiUrl + "TaskInfo/"

  constructor(private http: HttpClient) { }

  getProject(Id: string): Observable<ProjectInfo> {
    return this.http.get<ProjectInfo>(this.projectsPath + "GetById/" + Id);
  }

  getProjects(): Observable<Array<ProjectInfo>> {
    return this.http.get<Array<ProjectInfo>>(this.projectsPath + "GetFilteredList")
  }
  getProjectsByStatusId(statusId: string): Observable<Array<ProjectInfo>> {
    return this.http.get<Array<ProjectInfo>>(this.projectsPath + "GetFilteredList?" + `statusId=${statusId}`)
  }

  createProject(data: any): Observable<any> {
    return this.http.post<any>(this.projectsPath + "Create", data);
  }

  editProject(Id: string, Name: string, Priority:number): Observable<any> {
    return this.http.put<any>(this.projectsPath + `Edit/${Id}?` + `Name=${Name}` + `&Priority=${Priority}`, {})
  }

  deleteProject(Id: string): Observable<any> {
    return this.http.delete<any>(this.projectsPath + "Delete/" + Id);
  }

  completeProject(Id: string):Observable<any> {
    return this.http.post<any>(this.projectsPath + "CompleteProject/" + Id, {});
  }


  getTask(Id: string): Observable<TaskInfo> {
    return this.http.get<TaskInfo>(this.tasksPath + "GetById/" + Id)
  }

  getTasksByProjectId(Id: string): Observable<Array<TaskInfo>> {
    return this.http.get<Array<TaskInfo>>(this.tasksPath + "GetByProjectId/" + Id);
  }

  createTask(data: any): Observable<any> {
    return this.http.post<any>(this.tasksPath + "Create", data);
  }

  editTask(Id: string, Name: string, Description: string, Priority: number): Observable<any> {
    return this.http.put<any>(this.tasksPath + `Edit/${Id}?` + `Name=${Name}` + `&Description=${Description}` + `&Priority=${Priority}`, {});
  }
  
  deleteTask(Id: string): Observable<any> {
    return this.http.delete<any>(this.tasksPath + "Delete/" + Id);
  }

  taskInProgress(Id: string): Observable<any> {
    return this.http.post<any>(this.tasksPath + "TaskInProgress/" + Id, {});
  }

  taskDone(Id: string): Observable<any> {
    return this.http.post<any>(this.tasksPath + "TaskDone/" + Id, {});
  }

  getProjectStatuses(): Observable<Array<ProjectInfoStatus>> {
    return this.http.get<Array<ProjectInfoStatus>>(this.projectsPath + "GetProjectStatuses");
  }
}
