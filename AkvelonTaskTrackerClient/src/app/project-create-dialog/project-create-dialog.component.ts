import { Component, Inject, OnInit } from '@angular/core';
import { ProjectInfo } from '../models/ProjectInfo';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-project-create-dialog',
  templateUrl: './project-create-dialog.component.html',
  styleUrls: ['./project-create-dialog.component.css']
})
export class ProjectCreateDialogComponent implements OnInit {

  projectForm!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ProjectCreateDialogComponent>,
    private fb: FormBuilder,
    private httpService: HttpService) {
      this.projectForm = this.fb.group({
        'Name': [""],
        'Priority': [""]
      })
     }

  ngOnInit(): void {
  }

  cancel(): void {
    this.dialogRef.close();
  }

  create(): void {
    let Name = this.projectForm.get("Name")?.value;
    let Priority = this.projectForm.get("Priority")?.value;
    let data = new FormData();
    data.append("Name", Name);
    data.append("Priority", Priority);
    this.httpService.createProject(data).subscribe(res => {
      this.httpService.getProject(res.Id).subscribe(res => {
        this.dialogRef.close(res);
      })
    });
  }

}

