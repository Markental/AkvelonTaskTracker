import { Component, Inject, OnInit } from '@angular/core';
import { ProjectInfo } from '../models/ProjectInfo';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-project-edit-dialog',
  templateUrl: './project-edit-dialog.component.html',
  styleUrls: ['./project-edit-dialog.component.css']
})
export class ProjectEditDialogComponent implements OnInit {

  projectForm!: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ProjectEditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private fb: FormBuilder,
    private httpService: HttpService) {
      this.projectForm = this.fb.group({
        'Name': [data.selectedProject.Name],
        'Priority': [data.selectedProject.Priority]
      })
     }

  ngOnInit(): void {
  }

  cancel(): void {
    this.dialogRef.close();
  }

  edit(): void {
    let Name = this.projectForm.get("Name")?.value;
    let Priority = this.projectForm.get("Priority")?.value;

    this.httpService.editProject(this.data.selectedProject.Id, Name, Priority).subscribe(res => {
      this.dialogRef.close({Name, Priority});
    });
  }

}
export interface DialogData {
  selectedProject: ProjectInfo;
}
