﻿using AkvelonTaskTracker.Core.ContextEntities;
using AkvelonTaskTracker.Core.Models;
using AkvelonTaskTracker.Core.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AkvelonTaskTracker.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProjectInfoController : Controller
    {
        private readonly IProjectInfoService _projectInfoService;

        public ProjectInfoController(IProjectInfoService projectInfoService) 
        {
            _projectInfoService = projectInfoService;
        }

        [HttpGet]
        [Route(nameof(GetById) + "/{Id}")]
        public async Task<ProjectInfo> GetById([FromRoute] Guid id) 
        {
            var project = await _projectInfoService.GetOneById(id);

            return project;
        }

        [HttpGet]
        [Route(nameof(GetFilteredList))]
        public async Task<IEnumerable<ProjectInfo>> GetFilteredList(
            [FromQuery] string name,
            [FromQuery] Guid? statusId,
            [FromQuery] DateTime? dateFrom,
            [FromQuery] DateTime? dateTo,
            [FromQuery] int? skip,
            [FromQuery] int? top)
        {
            // Page
            if (skip == null)
            {
                skip = -1;
            }

            // Max elements on page
            if (top == null)
            {
                top = -1;
            }

            var result = await _projectInfoService.GetFilteredList(name, statusId, dateFrom, dateTo, skip.Value, top.Value);

            return result.Collection;
        }

        /// <summary>
        /// Create new Project
        /// </summary>
        /// <param name="model">Model with Name, StartDate and Priority fields</param>
        /// <returns>Id of created project</returns>
        [HttpPost]
        [Route(nameof(Create))]
        public async Task<ActionResult<Guid>> Create([FromForm] ProjectVM model) 
        {
            var projectId = await _projectInfoService.Create(model.Name, model.Priority);

            return CreatedAtAction(nameof(Create), new { Id = projectId });
        }

        [HttpPut]
        [Route(nameof(Edit) + "/{Id}")]
        public async Task<ActionResult> Edit(
            [FromRoute] Guid Id,
            [FromQuery] string Name,
            [FromQuery] int? Priority)
        {
            var isChanged = await _projectInfoService.Edit(Id, Name, Priority.Value);

            if (!isChanged) 
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpDelete]
        [Route(nameof(Delete) + "/{Id}")]
        public async Task<ActionResult> Delete([FromRoute] Guid Id) 
        {
            var result = await _projectInfoService.Delete(Id);

            if (result) 
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpPost]
        [Route(nameof(CompleteProject) + "/{Id}")]
        public async Task<ActionResult> CompleteProject([FromRoute] Guid Id) 
        {
            var result = await _projectInfoService.MarkAsComplete(Id);

            if (result)
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpGet]
        [Route(nameof(GetProjectStatuses))]
        public async Task<IEnumerable<ProjectInfoStatus>> GetProjectStatuses() 
        {
            var statuses = await _projectInfoService.GetProjectStatuses();

            return statuses;
        }
    }
}
