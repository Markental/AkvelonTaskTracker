﻿using AkvelonTaskTracker.Core.ContextEntities;
using AkvelonTaskTracker.Core.Models;
using AkvelonTaskTracker.Core.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AkvelonTaskTracker.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TaskInfoController : Controller
    {
        private readonly ITaskInfoService _taskInfoService;

        public TaskInfoController(ITaskInfoService taskInfoService)
        {
            _taskInfoService = taskInfoService;
        }

        [HttpGet]
        [Route(nameof(GetById) + "/{Id}")]
        public async Task<TaskInfo> GetById([FromRoute] Guid id)
        {
            var project = await _taskInfoService.GetOneById(id);

            return project;
        }

        [HttpGet]
        [Route(nameof(GetByProjectId) + "/{Id}")]
        public async Task<IEnumerable<TaskInfo>> GetByProjectId([FromRoute] Guid Id)
        {
            var tasks = await _taskInfoService.GetTasksByProjectId(Id);

            return tasks;
        }

        [HttpPost]
        [Route(nameof(Create))]
        public async Task<ActionResult<Guid>> Create([FromForm] TaskVM model) 
        {
            var taskId = await _taskInfoService.Create(model.ProjectId, model.Name, model.Description, model.Priority);

            return CreatedAtAction(nameof(Create), new { Id = taskId });
        }

        [HttpPut]
        [Route(nameof(Edit) + "/{Id}")]
        public async Task<ActionResult> Edit([FromRoute] Guid Id,
            [FromQuery] string Name,
            [FromQuery] string Description,
            [FromQuery] int? Priority)
        {
            var isChanged = await _taskInfoService.Edit(Id, Name, Description, Priority.Value);

            if (!isChanged)
            {
                return BadRequest();
            }

            return Ok();
        }

        [HttpDelete]
        [Route(nameof(Delete) + "/{Id}")]
        public async Task<ActionResult> Delete([FromRoute] Guid Id) 
        {
            var result = await _taskInfoService.Delete(Id);

            if (result)
            {
                return Ok();
            }

            return NotFound();
        }

        /// <summary>
        /// Change status of a Task to "In Progress" and assign Start Date to Project that is related to this Task
        /// </summary>
        /// <param name="Id">TaskInfo Id</param>
        /// <returns>Ok or NotFound ActionResult</returns>
        [HttpPost]
        [Route(nameof(TaskInProgress) + "/{Id}")]
        public async Task<ActionResult> TaskInProgress([FromRoute] Guid Id)
        {
            var result = await _taskInfoService.MarkAsInProgress(Id);

            if (result)
            {
                return Ok();
            }

            return NotFound();
        }

        /// <summary>
        /// Change status of a Task to "Done"
        /// </summary>
        /// <param name="Id">TaskInfo Id</param>
        /// <returns>Ok or NotFound ActionResult</returns>
        [HttpPost]
        [Route(nameof(TaskDone) + "/{Id}")]
        public async Task<ActionResult> TaskDone([FromRoute] Guid Id)
        {
            var result = await _taskInfoService.MarkAsDone(Id);

            if (result)
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpGet]
        [Route(nameof(GetTaskStatuses))]
        public async Task<IEnumerable<TaskInfoStatus>> GetTaskStatuses()
        {
            var statuses = await _taskInfoService.GetTaskStatuses();

            return statuses;
        }
    }
}
