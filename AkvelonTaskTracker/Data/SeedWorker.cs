﻿using AkvelonTaskTracker.Core.ContextEntities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AkvelonTaskTracker.Data
{
    public class SeedWorker : IHostedService
    {
        private readonly IServiceProvider _serviceProvider;
        private ApplicationDbContext _context;

        public SeedWorker(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            using var scope = _serviceProvider.CreateScope();

            _context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            await _context.Database.EnsureCreatedAsync(cancellationToken);

            await CreateStatuses();
        }

        // Insert statuses into database if they don't exist yet
        private async Task CreateStatuses() 
        {
            var hasStatuses = _context.TaskStatuses.Any();
            if (!hasStatuses) 
            {
                _context.TaskStatuses.Add(new TaskInfoStatus
                {
                    Name = "To Do",
                    Flag = Core.Abstractions.Enums.TaskStatusEnum.ToDo,
                });
                _context.TaskStatuses.Add(new TaskInfoStatus
                {
                    Name = "In Progress",
                    Flag = Core.Abstractions.Enums.TaskStatusEnum.InProgress,
                });
                _context.TaskStatuses.Add(new TaskInfoStatus
                {
                    Name = "Done",
                    Flag = Core.Abstractions.Enums.TaskStatusEnum.Done,
                });
            }

            hasStatuses = _context.ProjectStatuses.Any();
            if (!hasStatuses)
            {
                _context.ProjectStatuses.Add(new ProjectInfoStatus
                {
                    Name = "Not Started",
                    Flag = Core.Abstractions.Enums.ProjectStatusEnum.NotStarted,
                });
                _context.ProjectStatuses.Add(new ProjectInfoStatus
                {
                    Name = "Active",
                    Flag = Core.Abstractions.Enums.ProjectStatusEnum.Active,
                });
                _context.ProjectStatuses.Add(new ProjectInfoStatus
                {
                    Name = "Completed",
                    Flag = Core.Abstractions.Enums.ProjectStatusEnum.Completed,
                });
            }

            await _context.SaveChangesAsync();
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;
    }
}
